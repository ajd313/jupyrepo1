# %% Init
import matplotlib.pyplot as plt
import numpy as np
import os

# %%


def rotz(tta):
    return np.array([[np.cos(tta), -np.sin(tta), 0],
                     [np.sin(tta), np.cos(tta), 0],
                     [0, 0, 1]])


def S(v):
    # skew symmetric matrix
    return np.array([[0, -v[2], v[1]],
                     [v[2], 0, -v[0]],
                     [-v[1], v[0], 0]])


def frame2D(pbl=np.array([0, 0, 0]), Rbl=np.eye(3), name='', length=1):
    arrow_params = {'width': 0.1, 'length_includes_head': True, 'fill': True}
    xl = np.array([length, 0, 0])
    yl = np.array([0, length, 0])
    xb = Rbl @ xl
    yb = Rbl @ yl
    plt.arrow(pbl[0], pbl[1], xb[0], xb[1], color='blue', **arrow_params)
    plt.arrow(pbl[0], pbl[1], yb[0], yb[1], color='green', **arrow_params)
    # labels
    txt_x = Rbl @ (xl+np.array([-1, -0.8, 0])) + pbl
    txt_y = Rbl @ (yl+np.array([-0.8, -1, 0])) + pbl
    txt_base = Rbl @ np.array([-0.1, -0.9, 0]) + pbl
    plt.text(txt_x[0], txt_x[1], 'x', size=14, color='blue')
    plt.text(txt_y[0], txt_y[1], 'z', size=14, color='green')
    plt.text(txt_base[0], txt_base[1], name, size=16,)


def arrow_desc(p0, p, label, color='orange', txt_side=1):
    if np.linalg.norm(p) > 0:
        arrow_params = {'width': 0.1,
                        'length_includes_head': True, 'fill': True}
        plt.arrow(p0[0], p0[1], p[0], p[1], color=color, **arrow_params)
        txt_p = p0 + p/2 + (S(p/np.linalg.norm(p)) @
                            np.array([0, 0, txt_side])) * 0.5
        plt.text(txt_p[0], txt_p[1], label, size=14, color=color)

# %%


def movement_of_point2D(pba, Rba, pag, nuaa=np.array([2, -1, 0]), Fb=np.array([2, -1, 0]), draw_force=False, save=False):
    ax = plt.axes(xlim=(-1, 10), ylim=(-1, 10))
    plt.gcf().set_size_inches(7, 7)
    for s in ax.spines:
        ax.spines[s].set_color('black')
    ax.tick_params(axis='x', colors='black')
    ax.tick_params(axis='y', colors='black')
    # base frame - b
    frame2D(length=9, name='b')
    # local frame - a
    frame2D(length=3, pbl=pba, Rbl=Rba, name='a')
    # pba
    arrow_desc(np.array([0, 0, 0]), pba, r'$p^b_a$')
    # pag
    bpag = Rba @ pag
    arrow_desc(pba, bpag, r'$p^a_g$')
    # pbg
    pbg = pba+bpag
    arrow_desc(np.array([0, 0, 0]), pbg, r'$p^b_g$')
    # nuaa
    arrow_desc(pba, Rba @ nuaa, r'$\nu^a_a$', 'red')
    # omaa
    plt.plot(pba[0], pba[1], 'go')
    plt.text(pba[0]-0.5, pba[1]-1, r'$\omega^a_a$', size=14, color='green')
    # g
    if np.linalg.norm(pag) > 0:
        plt.text(pbg[0]+0.3, pbg[1]+0.3, 'g', size=14, color='red')
    plt.plot(pbg[0], pbg[1], 'ro')
    # F
    if draw_force:
        arrow_desc(pbg, Fb, r'$F$', 'magenta')

    if save:
        plt.savefig(os.path.join(os.getcwd(), 'gfx',
                                 f'{save}.svg'), bbox_inches='tight', transparent=True)
    plt.show()

# movement_of_point2D(np.array([6, 4, 0]), rotz(np.pi/4), np.array([2, 3, 0]))

# %%


def movement_of_rigid_body2D(pba, Rba, pai,
                             nuaa=np.array([2, -1, 0]), Fb=np.array([-2, 1, 0]), draw_force=False, save=False,
                             **kwargs):
    ax = plt.axes(xlim=(-1, 10), ylim=(-1, 10))
    plt.gcf().set_size_inches(7, 7)
    for s in ax.spines:
        ax.spines[s].set_color('black')
    ax.tick_params(axis='x', colors='black')
    ax.tick_params(axis='y', colors='black')
    # base frame - b
    frame2D(length=9, name='b')
    # local frame - a
    frame2D(length=3, pbl=pba, Rbl=Rba, name='a')
    # pba
    arrow_desc(np.array([0, 0, 0]), pba, r'$p^b_a$')
    # pai
    bpai = Rba @ pai
    arrow_desc(pba, bpai, r'$p^a_i$')
    # pb
    pbi = pba+bpai
    arrow_desc(np.array([0, 0, 0]), pbi, r'$p^b_i$')
    # nuaa
    arrow_desc(pba, Rba @ nuaa, r'$\nu^a_a$', 'red')
    # omaa
    plt.plot(pba[0], pba[1], 'go')
    plt.text(pba[0]-0.5, pba[1]-1, r'$\omega^a_a$', size=14, color='green')
    # Vi
    if np.linalg.norm(pai) > 0:
        plt.plot(pbi[0], pbi[1], color='r', marker='s',
                 fillstyle='none', ms=15, mew=3)
        plt.text(pbi[0]+0.4, pbi[1]+0.4, r'$V_i$', size=14, color='red')
    # body shape
    body = plt.Polygon(
        np.array([[-2, -2], [-3, -1], [-2, 1], [-3, 3], [-1, 5],
                  [2, 3], [3, 1], [2, 0], [3, -2], [1, -3]])
        + pba[0:2],
        closed=True,
        alpha=0.4,
        color='cyan')
    ax.add_patch(body)
    # F
    if draw_force:
        arrow_desc(pbi, Fb, r'${}^i F$', 'magenta')

    if 'arrows' in kwargs:
        for arrow in kwargs['arrows']:
            d = {'p0': [0, 0, 0], 'p': [0, 0, 0],
                 'label': '', 'color': 'orange', 'txt_side': 1}
            d.update(arrow)
            arrow_desc(np.array(d['p0']), np.array(d['p']),
                       d['label'], d['color'], d['txt_side'])

    if save:
        plt.savefig(os.path.join(os.getcwd(), 'gfx',
                                 f'{save}.svg'), bbox_inches='tight', transparent=True)
    plt.show()

# extra = {'arrows': [{'p0':[1,2,0],'p':[3,3,0],'label':r'extra','color':'pink'}]}
# movement_of_rigid_body2D(np.array([6, 4, 0]), rotz(np.pi/4), np.array([2, 3, 0]), draw_force=True, **extra)
# %%


# %%
